// Criando nosso nó
#include <stdlib.h>
#include <stdio.h>
typedef struct node{
  int valor;
  struct node *esq;
  struct node *dir;
} node_t;

void inserir(node_t *arvore, int valor){
  if(arvore->valor == 0){
    // Começando com  básico, inserindo o valor no nó atual
    // A posição está vázia
    arvore->valor = valor;
  }else{
    if(valor < arvore->valor){
      if(arvore->esq != NULL) // CASO(IF) nosso nó da esquerda esteja criado, ele vai inserir o valor nele...
      // de forma recursiva!
      inserir(arvore->esq, valor);
    }else { // Senão, nós vamos criar o nó da esquerda da árvore
      // BEM AQUI...
      arvore->esq = (node_t*) malloc(sizeof(node_t));
      // Setando os valores explicitamente
      arvore->esq->valor = valor;
      arvore->esq->esq = NULL;
      arvore->esq->dir = NULL;
    }
    if(valor >= arvore->valor){
      // Inserindo a direita
      // Vamos fazer exatamente a mesma coisa que fizemos com a esquerda!
      if(arvore->dir != NULL){ // Verificando a existencia do nó direito
        inserir(arvore->dir, valor); // Caso ele exista, inserimos o valor de forma recursiva!
      }else {
        arvore->dir = (node_t *) malloc(sizeof(node_t));
        // Apenas alocando espaço para os próximos nós que possam vir a existir
        arvore->dir->esq = NULL;
        arvore->dir->dir = NULL;
      }
    }
  }
}

// Vamos imprimir cada elemento da árvore de forma recursiva
void ler_arvore(node_t * no_atual){
  // Primeiro vamos tomar umas medidas de segurança caso o nó seja um ponteiro nulo
  if(no_atual == NULL) return;
  if(no_atual->esq != NULL) ler_arvore(no_atual->esq);
  if(no_atual != NULL) printf("%d", no_atual->valor);
  if(no_atual != NULL) ler_arvore(no_atual->dir);
}

int main() {
  node_t * test_list = (node_t *) malloc(sizeof(node_t));
  // Setandos os valores
  test_list->valor = 0; // Esse é o valor o primeiro nó
  // Os nós filhos
  test_list->esq = NULL;
  test_list->dir = NULL;

  inserir(test_list, 5);
  inserir(test_list, 8);
  inserir(test_list, 4);
  inserir(test_list, 3);

  // Lendo os valores inseridos
  ler_arvore(test_list);
  printf("\n");
}
